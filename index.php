<!DOCTYPE html>
<?php

/**
 *  include the necessary files
 */
include_once "./utils/utils.php";
include_once './classes/product.php';
include_once './connection/db_connection.php';


/** @var mysqli $conn open the connection whit the database */
$conn = OpenCon();

/**@var Product $product calls the Product class to delete data */
$product = new Product;

$sql = new SQL;

$list = new listPro;


/** if some information come from POST whit the name array */
if (isset($_POST["array"])) {

  /** @var array $array recive the array from the utils.js and transform them in an php array */
  $array = explode(",", $_POST["array"]);

  /** call the steList() setter to pass the array */
  $list->setList($array);

  /** calls the massDelete() function*/
  $list->delete();
  
}
?>
<html>

<head>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>

  <title>Scandiweb Junior Test</title>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' type='text/css' media='screen' href='./css/style.css'>
  <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
  <script src='./utils/utils.js'></script>
  <!-- Bootstrap css -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>

  <!-- Top Buttons -->
  <div class="container">
    <div class="row">
      <div class="col-9">
        <p class="page-title">Product List</p>
      </div>
      <div class="col">
        <a href="./add-product.php" class="btn btn-primary">ADD</a>
      </div>
      <div class="col-2">
        <input type="button" class="btn btn-primary" onclick="massDelete();
        setTimeout(function(){window.location.reload();}, 500);" value="MASS DELETE">
      </div>
    </div>
    <hr />
  </div>



  <div class="container">
    <div class="row row-cols-sm-4 g-sm-3">
     
      <?php

      /** TODO comment section   */
      $result = $sql->select();

      /** This is used to iterate inside de PRODUCT table */
        // output data of each row
        while ($row = $result->fetch_assoc()) {
      ?>

          <div class="col-sm">
            <div class="card text-center">
              <div class="card-body">
                <input class="delete-checkbox" type="checkbox" value="<?php echo $row["SKU"] ?>">
                <br>
                <h5 class="card-title"><?php echo $row["SKU"] ?></h5>
                <p class="card-text"><?php echo $row["NAME"] ?></p>
                <p class="card-text"><?php echo $row["PRICE"] ?> $</p>
                <div>
                  <p class="card-text" id="DVD<?php echo $row["SKU"] ?>" style="display: none;"> SIZE <?php echo $row["ATRIBUTED"] ?> (MB)</p>
                  <p class="card-text" id="Book<?php echo $row["SKU"] ?>" style="display: none;">WEIGTH <?php echo $row["ATRIBUTEB"] ?> (KG)</p>
                  <p class="card-text" id="Furniture<?php echo $row["SKU"] ?>" style="display: none;">DIMENSIONS <?php echo $row["ATRIBUTEH"] ?>x<?php echo $row["ATRIBUTEW"] ?>x<?php echo $row["ATRIBUTEL"] ?> </p>
                  <Script>
                    changeC("<?php echo $row["TYPE"] ?>", "<?php echo $row["SKU"] ?>");
                  </Script>
                </div>
              </div>
            </div>
          </div>
      <?php
        }
      ?>
    </div>
  </div>

  <footer class="footer">
    <div class="container">
      <hr />
      <p class="text-center">Scandiweb Test assignment
      <p>
    </div>
  </footer>
  <!-- Bootstrap js -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>



</html>