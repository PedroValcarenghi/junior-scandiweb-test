/**
 *  This function is used to call the gather the SKUs to delete
 *  
 *  @var Array array This is used to stre the SKUS
 *  
 *  @var Array checkboxes This is used to gather the checkboxes values
 * 
 *  @returns POST
 */
function massDelete() {
    /**@var Array array This is used to stre the SKUS */
    var array = []

    /**@var Array checkboxes This is used to gather the checkboxes values */
    var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')

    /** Put the checkboxes values inside the array */
    for (var i = 0; i < checkboxes.length; i++) {
        array.push(checkboxes[i].value)
    }

    array = array.join(',')

    /** send the array */
    if ($.post("./index.php", { 'array': array })) {
        console.log('send !')
    }
}

/** This Function shows the inputs for the types of product */
function atribute(value) {

    /** Hide all types. */
    document.getElementById("DVD").style = "display: none;";
    document.getElementById("Book").style = "display: none;";
    document.getElementById("Furniture").style = "display: none;";

    /** Show the selected input type */
    document.getElementById(value).style = "display: initial;";
}

/** change the card description based in the product type 
 * 
 * @var string a string that recive the type of the product
 * @var string b string that recevi the SKU of the prudct
 * 
 */
function changeC(a, b) {
    /** get element based in the id of the div where a = type and b = SKU */
    document.getElementById(a + b).style = "display: initial;";

}