<?php

include './classes/product.php';
include_once "connection/db_connection.php";




// /**@var Product $product calls the Product class to delete data */
// $product = new Product;

// /** if some information come from POST whit the name array */
// if (isset($_POST["array"])) {

//   /** @var array $array recive the array from the utils.js and transform them in an php array */
//   $array = explode(",", $_POST["array"]);

//   /** call the steList() setter to pass the array */
//   $product->setList($array);

//   /** calls the massDelete() function and if it delete calls an echo Deleted */
//   if ($product->massDelete()) {
//     echo "Deleted";
//     header("url=./index.php");

//   }
// }


class SQL
{

  public $conn;

 public $product;

  /** This is a function of contruction*/
  public function __construct()
  {
    $this->conn = OpenCon();

    $this->product = new Product;
  }


  public function select()
  {
    /** @var string $sql string to make the query */
    $sql = "SELECT * FROM PRODUCT ORDER BY SKU";
    /** @var /mysqli $result This is the query result  */
    $result = $this->conn->query($sql);

    return ($result);
  }

  /**
   *  This is a function for insert data in the database 
   * 
   * @var string $string recive the mysql query as a string
   * @var mysqli $prep prepare the $string to be called as a query
   * @var mysqli $insert execute the query from $prep
   * 
   * @return mysqli
   *      
   */
  public function insert()
  {

    /** @var string $string recive the mysql query as a string. */
    $string = "INSERT INTO `PRODUCT`(`SKU`, `NAME`, `PRICE`, `TYPE`, `ATRIBUTED`, `ATRIBUTEB`, `ATRIBUTEL`, `ATRIBUTEW`,`ATRIBUTEH`) VALUES ('{$this->product->getSku()}','{$this->product->getName()}','{$this->product->getPrice()}','{$this->product->getType()}','{$this->product->getAtributed()}','{$this->product->getAtributeb()}','{$this->product->getAtributel()}','{$this->product->getAtributew()}','{$this->product->getAtributeh()}')";
    /** @var /mysqli $prep prepare the $string to be called as a query. */
    $prep = mysqli_prepare($this->conn, $string);
    /** @var mysqli $insert execute the query from $prep. */
    $insert = $prep->execute();

    return $insert;
  }
}


class listPro
{
  /** This is a function setter for the $list */
  function setList($list)
  {
    $this->list = $list;
  }
  /** This is a function getter for the $list */
  function getlist()
  {
    return $this->list;
  }


  /**
   *  This is a function for delete data in the database 
   * 
   * @var array $array var for the array of the sku for delete
   * @var int $cont cont the number of sku that is for delete
   *
   * @var string $stringDlt recive the mysql query as a string for delete
   * @var mysqli $prep prepare the $string to be called as a query
   * @var mysqli $dlt execute the query from $prep
   * 
   * @return string
   *      
   */
  public function delete()
  {
    /** @var array $array This is the array of the sku for delete */
    $array = $this->getlist();

    $cont = sizeof($this->getlist());

    for ($i = 0; $i < $cont; $i++) {
      /** @var string $stringDlt recive the mysql query as a string for delete */
      $stringDlt = "DELETE FROM PRODUCT where SKU IN ('{$array[$i]}') ";
      /** @var /mysqli $prep prepare the $string to be called as a query */
      $prep = mysqli_prepare($this->conn, $stringDlt);
      /**@var mysqli $dlt execute the query from $prep */
      $dlt = $prep->execute();
    }

    return ("Deleted");
  }

}
