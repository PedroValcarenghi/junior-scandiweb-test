<?php

/**
 * This is a Function to connect to the database.
 * 
 * @var string $dbhost host name for the database.
 * @var string $dbuser username for the database.
 * @var string $dbpass password name for the database.
 * @var string $db  database name.
 * @var mysqli $conn open the connection whit the database.
 * 
 * @return mysqli
 * 
 */
#Open Database Connection
function OpenCon()
{
    /** @var string $dbhost host name for the database.*/
    $dbhost = "localhost";
    /** @var string $dbuser username for the database.*/
    $dbuser = "root";
    /** @var string $dbpass password name for the database. */
    $dbpass = "1234";
    /** @var string $db  database name. */
    $db = "ScandiwebJuniorTest";

    /** @var mysqli $conn open the connection whit the database. */
    $conn = new mysqli($dbhost, $dbuser,  $dbpass, $db) or die("Connect failed: %s\n" . $conn->error);

    return $conn;
}

/**
 * This is a function to close the connection whit the database. *   
 */
#Close Database Connection
function CloseCon($conn)
{
    /** @var mysqli $conn connection whit the database */
    $conn->close();
}
