<?php

/** include of the connection file */
include './connection/db_connection.php';

/**
 * @class This is the class of the products handling getters,setters,insert and delete.
 */
class Product
{
    /** @var mysqli $conn This is a the variable for the msqli connection. */
    public $conn;

    /** @var int $cont This is a counter. */
    public $cont = 0;

    /** @var string $sku This the variable to getters and setter for the SKU in the database. */
    private $sku;

    /** @var string $name This the variable to getters and setter for the NAME in the database. */
    private $name;

    /** @var int $price This the variable to getters and setter for the PRICE in the database. */
    private $price;

    /** @var string $type This the variable to getters and setter for the TYPE in the database. */
    private $type;

    /** @var int $atributeb This the variable to getters and setter for the ATRIBUTEB in the database. */
    private $atributeb;

    /** @var int $atributed This the variable to getters and setter for the ATRIBUTED in the database. */
    private $atributed;

    /** @var int $priatributel This the variable to getters and setter for the ATRIBUTEL in the database. */
    private $atributel;

    /** @var int $atributeh This the variable to getters and setter for the ATRIBUTEH in the database. */
    private $atributeh;

    /** @var int $atributew This the variable to getters and setter for the ATRIBUTEW in the database. */
    private $atributew;

    /** @var array $list This is a list whit the sku from the checkedboxes in the index */
    private $list;

    /** This is a function getter for the $sku */
    function setSku($sku)
    {
        $this->sku = $sku;
    }
    /** This is a function getter for the $sku */
    function getSku()
    {
        return $this->sku;
    }

    /** This is a function setter for the $name */
    function setName($name)
    {
        $this->name = $name;
    }
    /** This is a function getter for the $name */
    function getName()
    {
        return $this->name;
    }

    /** This is a function setter for the $price */
    function setPrice($price)
    {
        $this->price = $price;
    }
    /** This is a function getter for the $price */
    function getPrice()
    {
        return $this->price;
    }


    /** This is a function setter for the $type */
    function setType($type)
    {
        $this->type = $type;
    }
    /** This is a function getter for the $type */
    function getType()
    {
        return $this->type;
    }

    /** This is a function setter for the $atributed */
    function setAtributed($atributed)
    {
        $this->atributed = $atributed;
    }
    /** This is a function getter for the $atributed */
    function getAtributed()
    {
        return $this->atributed;
    }

    /** This is a function setter for the $atributeb */
    function setAtributeb($atributeb)
    {
        $this->atributeb = $atributeb;
    }
    /** This is a function getter for the $atributeb */
    function getAtributeb()
    {
        return $this->atributeb;
    }

    /** This is a function setter for the $atributel */
    function setAtributel($atributel)
    {
        $this->atributel = $atributel;
    }
    /** This is a function getter for the $atributel */
    function getAtributel()
    {
        return $this->atributel;
    }

    /** This is a function setter for the $atributeh */
    function setAtributeh($atributeh)
    {
        $this->atributeh = $atributeh;
    }
    /** This is a function getter for the $atributeh */
    function getAtributeh()
    {
        return $this->atributeh;
    }

    /** This is a function setter for the $atributew */
    function setAtributew($atributew)
    {
        $this->atributew = $atributew;
    }
    /** This is a function getter for the $atributew */
    function getAtributew()
    {
        return $this->atributew;
    }




    /** This is a function of contruction*/
    public function __construct()
    {
        $this->conn = OpenCon();
    }

  
}
