<!DOCTYPE html>
<?php
/**
 * include the necessary files
 * 
 */
include_once "./utils/utils.php";
include_once './classes/product.php';
include_once './connection/db_connection.php';

/** @var mysqli $conn open the connection whit the database */
$conn = OpenCon();

/**@var Product $insertProduct calls the Product class to insert data */
$insertProduct = new Product;

$SQL = new SQL;

/** if used to insert data via Product class */
if (isset($_POST["submit"])) {

    /** @var string $sql string whit the query to veryficate if the sku already exist */
    $sql = "SELECT * FROM PRODUCT WHERE SKU = '{$_POST['sku']}'";
    /** @var mysqli $result result of the sql string query */
    $result = $conn->query($sql);

    /** if the $result has more than one row then show a alert */
    if ($result->num_rows > 0) {
?>
        <!-- <div class="alert alert-danger text-center" role="alert">
            Sku Already exist !
        </div> -->
<?php
    /** if the sku does not exist insert the data and calls the insert() function */
    } else {

        /** insert the data in the product setters */
        $insertProduct->setSku($_POST['sku']);
        $insertProduct->setName($_POST['name']);
        $insertProduct->setPrice($_POST['price']);
        $insertProduct->setType($_POST['productType']);

        $insertProduct->setAtributed($_POST['size']);

        $insertProduct->setAtributeb($_POST['weight']);

        $insertProduct->setAtributel($_POST['length']);
        $insertProduct->setAtributeh($_POST['height']);
        $insertProduct->setAtributew($_POST['width']);

       /** calls the insert() funtion and if it is sucesefull calls the index.php page */
        if ($SQL->insert()) {
            echo  "<script>window.location='./index.php';</script>";
        }
    }
}
?>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>add-product</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='./css/style.css'>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src='./utils/utils.js'></script>
</head>

<body>
    <form method="post" id="product_form">
        <!-- Header -->
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <p class="page-title">Product Add</p>
                </div>
                <div class="col">
                    <input type="submit" name="submit" class="btn btn-primary" value="Save" />
                </div>
                <div class="col-2">
                    <a href="./index.php" class="btn btn-primary">Cancel</a>
                </div>
            </div>
            <hr />
        </div>



        <!-- Form Camps -->
        <div class="container">
            <div class="col" input-group mb-3>
                <label for="SKU" class="input-group-text">SKU</label>
                <input type="text" name="sku" class="form-control" id="sku" required autofocus>
            </div>
            <div class="col" input-group mb-3>
                <label for="name" class="input-group-text">Name</label>
                <input type="text" name="name" class="form-control" id="name" required>
            </div>
            <div class="col input-group mb-3">
                <label for="price" class="input-group-text">Price</label>
                <span class="input-group-text">$</span>
                <input type="number" name="price" class="form-control" id="price" step="0.01" min="1" required>
            </div>

            <!-- Type Selection -->
            <div class="col">
                <label for="type" class="form-label">Type Switcher</label>

                <select name="productType" id="productType" class="form-select" onchange="atribute(value);" required>

                    <option value='DVD'>DVD</option>

                    <option value='Furniture'>Furniture</option>

                    <option value='Book'>Book</option>

                </select>
            </div>
            <br />

            <!-- DVD Atribute Section-->
            <div class="col" id="DVD" style="display: initial;">
                <label for="size" class="form-label">Size (MB)</label>
                <input type="number" name="size" class="form-inline-control" value="1" id="size" min="1"  required>
                <br />
                <label class="form-label">Please provide the size of the DVD !</label>
            </div>

            <!-- Furniture Atribute Section -->
            <div id="Furniture" style="display: none;">
                <div class="col">
                    <label class="form-label">Height (CM)</label>
                    <input type="number" name="height" class="form-inline-control" value="1" id="height" min="1"  required>
                </div>
                <div class="col">
                    <label class="form-label">Width (CM)</label>
                    <input type="number" name="width" class="form-inline-control" value="1" id="width" min="1" required>
                </div>
                <div class="col">
                    <label class="form-label">Length (CM) </label>
                    <input type="number" name="length" class="form-inline-control" value="1" id="length" min="1" required>
                </div>
                <label class="form-label">Please provide the dimension for the furniture !</label>
            </div>

            <!-- Book Atribute Section -->
            <div class="col" id="Book" style="display: none;">
                <label for="weight" class="form-label">Weight (KG)</label>
                <input type="number" name="weight" class="form-inline-control" value="1" id="weight" min="1"  required>
                <br />
                <label class="form-label">Please provide the weight of the book !</label>
            </div>


        </div>
    </form>
    <footer class="footer">
        <div class="container">
            <hr />
            <p class="text-center">Scandiweb Test assignment
            <p>
        </div>

    </footer>
</body>

</html>